// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'fetureScreens.dart';
import 'main.dart';

//Creating the dashboard screen which will also act as a Homescreen
class DashBoard extends StatefulWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DashBoard'),
        actions: [
          IconButton(
            icon: const Icon(Icons.exit_to_app_rounded),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext) {
                return LogInScreen();
              }));
            },
          ),
          IconButton(
            icon: const Icon(Icons.menu_book),
            onPressed: () {},
          )
        ],
      ),
      body: Container(
        // ignore: sort_child_properties_last
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Row(
            children: <Widget>[
              RaisedButton(
                shape: const RoundedRectangleBorder(
                    borderRadius:
                        BorderRadiusDirectional.all(Radius.circular(16.0))),
                child: const Text(
                  'Feature 1',
                  style: TextStyle(color: Colors.blueGrey, fontSize: 30),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) {
                    return FeatureScreen1();
                  }));
                },
              ),
              const SizedBox(
                width: 50,
              ),
              RaisedButton(
                shape: const RoundedRectangleBorder(
                    borderRadius:
                        BorderRadiusDirectional.all(Radius.circular(16.0))),
                child: const Text(
                  'Feature 2',
                  style: TextStyle(color: Colors.blueGrey, fontSize: 30),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) {
                    return FeatureScreen2();
                  }));
                },
              )
            ],
          ),
          const SizedBox(
            height: 100,
          ),
          Container(
            // ignore: sort_child_properties_last
            child: FloatingActionButton(
              child: const Text(
                'Profile',
                style: TextStyle(fontSize: 15),
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext) {
                  return EditScreen();
                }));
              },
            ),
            margin: const EdgeInsets.only(right: 280),
          )
        ]),
        color: Colors.black,
        alignment: Alignment.center,
        padding: const EdgeInsets.all(200),
        constraints: const BoxConstraints.tightForFinite(
          width: 1000,
        ),
      ),
    );
  }
}

//Creating a profile edit screen that which will.
class EditScreen extends StatefulWidget {
  const EditScreen({Key? key}) : super(key: key);

  @override
  State<EditScreen> createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> {
  late String _nameForEdit;
  late String _emailForEdit;
  late String _PasswordForEdit;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Name Is Required";
        }
      },
      onSaved: (String? value) {
        _nameForEdit = value!;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Email'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Email Is Required";
        }
      },
      onSaved: (String? value) {
        _emailForEdit = value!;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Password'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Password Is Required";
        }
      },
      onSaved: (String? value) {
        _PasswordForEdit = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: Container(
        margin: EdgeInsets.all(50),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              _buildName(),
              const SizedBox(
                height: 50,
              ),
              _buildEmail(),
              const SizedBox(
                height: 50,
              ),
              _buildPassword(),
              const SizedBox(
                height: 100,
              ),
              RaisedButton(
                shape: const RoundedRectangleBorder(
                    borderRadius:
                        BorderRadiusDirectional.all(Radius.circular(16.0))),
                child: const Text(
                  'Submit',
                  style: TextStyle(color: Colors.blue, fontSize: 16),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) {
                    return LogInScreen();
                  }));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}














/*class LogInScreen extends StatefulWidget {
  const LogInScreen({Key? key}) : super(key: key);

  @override
  State<LogInScreen> createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  late String _name;
  late String _email;
  late String _Password;
  late String _VerifyPassword;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Name Is Required";
        }
      },
      onSaved: (String? value) {
        _name = value!;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Email'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Email Is Required";
        }
      },
      onSaved: (String? value) {
        _email = value!;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Password'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Password Is Required";
        }
      },
      onSaved: (String? value) {
        _Password = value!;
      },
    );
    ;
  }
  /*Widget _buildVerifyPassword(){
    return return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Name Is Required";
        }
      },
      onSaved: (String? value) {
        _name = value!;
      },
    );;
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome'),
      ),
      body: Container(
        margin: EdgeInsets.all(50),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              _buildName(),
              const SizedBox(
                height: 50,
              ),
              _buildEmail(),
              const SizedBox(
                height: 50,
              ),
              _buildPassword(),
              const SizedBox(
                height: 100,
              ),
              RaisedButton(
                child: const Text(
                  'Submit',
                  style: TextStyle(color: Colors.blue, fontSize: 16),
                ),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    return;
                  }
                  _formKey.currentState!.save();

                  print(_name);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}*/